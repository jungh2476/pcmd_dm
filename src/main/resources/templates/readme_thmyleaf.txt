https://jongminlee0.github.io/2020/03/12/thymeleaf/

 ${…}
선택 변수 : *{…}
메시지 : #{…}
Link URL : @{…}

<link th:href="@{/css/index.css}" rel="stylesheet" type="text/css">

<tbody>
    <tr th:object="${board}">
        <td><span th:text="*{no}"></span></td>
        <td><span th:text="*{title}"></span></td>
        <td><span th:text="*{writer}"></span></td>
        <td><span th:text="${#temporals.format(board.updateTime, 'yyyy-MM-dd HH:mm')}"></span></td>
    </tr>
</tbody>
#objects : 일반적인 객체를 다룬다.
#bools : boolean을 위해 사용.
#arrays : 배열을 위해 사용.
#lists : 리스트를 위한 유틸리티 메소드.
#sets : set을 위한 유틸리티 메소드.
#maps : map을 위한 유틸리티 메소드.
<tbody>
    <tr th:each="board, iterState : ${boardList}">
        <td><span th:text="${iterState.index}"></span></td>
        <td><span th:text="${board.title}"></span></td>
        <td><span th:text="${board.writer}"></span></td>
        <td><span th:text="${#temporals.format(board.updateTime, 'yyyy-MM-dd HH:mm:ss')}"></span></td>
    </tr>
</tbody>

?: 1개 글자
*: 0개 이상의 글자
**:0개 혹은 그 이상의 디렉토리 경로  /folders/**/files => /folders/files, /folders/1/2/3/files

---------------------
@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(NoLoginException.class)
    public String noLoginException(Exception e, Model model) {
        model.addAttribute("errorMessage", e.getMessage());
        return "user/login_failed";
    }
}

@RestControllerAdvice
public class RestExceptionHandler {
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
  @ExceptionHandler(value = UnAuthorizedException.class)
  public Object handleUnAutorizedException(UnAuthorizedException e) {
    return new Object(e.getMessage());
  }
}

@ExceptionHandler(NullPointerException.class) 
public Object nullex(Exception e) {    //@ExceptionHandler({ Exception1.class, Exception2.class}) 이런식으로 두 개 이상 등록도 가능
	System.err.println(e.getClass()); 
	return "myService"; 
}

Controller, RestController에만 적용가능하다. (@Service같은 빈에서는 안됨.)

@ControllerAdvice는 모든 @Controller 즉, 전역에서 발생할 수 있는 예외를 잡아 처리해주는 annotation
@ControllerAdvice
@RestController에서 예외가 발생하든 @Controller에서 예외가 발생하든 @ControllerAdvice + @ExceptionHandler 조합으로 다 캐치할 수 있고
만약에 전역의 예외를 잡긴하되 패키지 단위로 제한할 수도있다.
@RestControllerAdvice("com.example.demo.login.controller")
@RestControllerAdvice 은 @ControllerAdvice 어노테이션과 @ResponseBody 어노테이션을 합쳐놓은 어노테이션이다
@﻿ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
출처: https://jeong-pro.tistory.com/195 [기본기를 쌓는 정아마추어 코딩블로그]



method(@ModelAttribute("test") Test test, Model model)

이것이 항상 머저 호출되어져 model에 탑재되어져 있다 
@ModelAttribute
public void addAttributes(Model model) {
    model.addAttribute("msg", "Welcome to the Netherlands!");
}

@RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
public String submit(@ModelAttribute("employee") Employee employee) {
    // Code that uses the employee object

    return "employeeView";
}

BeanUtils.copyProperties(form, customer);

return "redirect:/customers"




@RequestMapping({"/main/",""})   or (value={"","abc*""view/*,**/msg"}, headers="content-type=text/plain"})
@RequestMapping({"/"})
RequestMapping(value="fetch/{id})
String method(@PathVairable String id)

(value="/fetch/{category:[a-z}+}/{name}" => /fetch/category/name
@RequestMapping()  //default method




----------------------------
Jackson
@JsonIgnoreProperties(ignoreUnknown = true)
class level

@JsonIgnore  - 이 항목 안 내보냄 
