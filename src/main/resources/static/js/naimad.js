var isMenuShown = false;
var preventMenuEvent = false;

$(document).ready(function() {
    initMenuBtn();
});

function initMenuBtn() {
    
    $('.menu-btn').click(function() {
        if (preventMenuEvent) return;
        if (isMenuShown) hideMenu();
        else showMenu();
    });

    //

    // 1) down animation
    // 1-2) menu icon change
    // 2) text opaicty animation
    function showMenu() {
        preventMenuEvent = true;
        isMenuShown = true;

        $('.menu').animate({
            top: 0
        }, 500, function() {
            $('.menu').addClass('shown');
        });
        $('.menu-btn').addClass('close');
        
        setTimeout(function() {
            preventMenuEvent = false;
        }, 1000);
    }

    // 1) text opaicty animation
    // 1-2) menu icon change
    // 2) up animation
    function hideMenu() {
        preventMenuEvent = true;
        isMenuShown = false;

        $('.menu').removeClass('shown');
        $('.menu-btn').removeClass('close');
        setTimeout(function() {
            $('.menu').animate({
                top: '-100%'
            }, 500);
        }, 500);

        setTimeout(function() {
            preventMenuEvent = false;
        }, 1000);
    }

}