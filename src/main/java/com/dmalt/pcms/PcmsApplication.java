package com.dmalt.pcms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.EnableAsync;

import com.dmalt.pcms.core.events.OrderEvent;



@EnableAsync
@SpringBootApplication
public class PcmsApplication implements CommandLineRunner {
	
	static Logger logger = LoggerFactory.getLogger(PcmsApplication.class);
	
	@Autowired
	ApplicationEventPublisher appEventPublisher;
	
	//@Value("hello")
	@Value("${spring.start.message: some default}")
	private String message1;
	
	//@Value("#{${valuesMap: {key1: '1', key2: '2'}}}")
	//private Map<String, Integer> valuesMap;
	
	//@Autowired private Environment env;
	//env.getProperty("spring.start.message")
	//@PostConstruct private void init(){}
	
	public static void main(String[] args) {
		SpringApplication.run(PcmsApplication.class, args);
		logger.info("starting.....hi:");
		
	}

	@Override
	public void run(String... args) throws Exception {
		// 실험, 초기화 관련 자료 들을 설정해 둔다 
		String[] sargs;
		logger.info("starting.....hi:"+message1);
		logger.info("starting.....commandline runner hi");
		logger.info("publishing event ...I am order event");
		OrderEvent oevent = new OrderEvent("I am order event");
		appEventPublisher.publishEvent(oevent);
		
		
	}

}

