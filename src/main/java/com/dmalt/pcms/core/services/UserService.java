package com.dmalt.pcms.core.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dmalt.pcms.core.models.User;
import com.dmalt.pcms.core.repositories.UserRepository;

@Service
public class UserService {
	
	Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserRepository uRepo;
	
	@Transactional
	public User findById(Long id) {
		User u=uRepo.findById(id).get();
		logger.info("found user by id");
		logger.info("user scopes size:"+u.scopes.size());
		return u;
	}

}
