package com.dmalt.pcms.core.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dmalt.pcms.core.models.Comment;
import com.dmalt.pcms.core.models.QComment;
import com.dmalt.pcms.core.models.QUser;
import com.dmalt.pcms.core.models.converters.ListStringConverter;
import com.dmalt.pcms.core.models.dtos.CommentDto;
import com.dmalt.pcms.core.repositories.CommentRepository;
import com.dmalt.pcms.core.repositories.CommentRepositorySupport;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

@Service
public class CommentService {
	
	Logger logger = LoggerFactory.getLogger(CommentService.class);

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	CommentRepository commentRepo;
	
	@Autowired
	CommentRepositorySupport commentRepoSupport;
	
	//이렇게 밖으로 꺼내서 쓸수가 없음 
	//JPAQuery<Comment> query = new JPAQuery<>(em);
	//QComment comment = QComment.comment;
	//QUser user = QUser.user;
	
	//이렇게 직접 호출해서 연산을 해도 되지만 되도록이면 복잡한것은 commentRepoSupport에 method로 만들어 둔다
	public List<Comment> findByBodytextDescriptionQueryDSL(){
		JPAQuery<Comment> query = new JPAQuery<>(em);
		QComment comment = QComment.comment;
		List<Comment> comments2 = query.from(comment).where(comment.bodytext.contains("description")).fetch();
		logger.info("2nd queryDSL returned size2:"+comments2.size());
		return comments2;
	}
	
	public List<CommentDto> findAllByTagDTO(String tag, String userid, String ugroupid){
		JPAQuery<Comment> query = new JPAQuery<>(em);
		QComment comment = QComment.comment;
		QUser user = QUser.user;
		//List<CommentDto> resultCommentDtos = null;
		List<String> ugroups = new ArrayList<>();
		ugroups.add("3");
		ugroups.add("5");
		//String ugroupsString = ListStringConverter.toStringStringList(ugroups);
		List<CommentDto> resultCommentDtos = query.select(Projections.constructor(CommentDto.class, comment.tags, comment.bodytext,user.name ))
				.from(comment).innerJoin(user).on(comment.authorid.eq(user.id)).where(getBuilder(false, userid, ugroups)).fetch(); //on where //getNotSnapshot().and(getReadAccessUser("o d"))
		logger.info("method x inner values:rausers:"+comment.rausers.toString());
		//if(comment.rausers instanceof List) {logger.info("size of rausers:"comment.ra);}
		
		//String uname,List<String> tags,String bodytext
		/*
		
		DTO 적용 예시
		복합쿼리 적용 예시 
		Boolean Expression
		
		*/
		return resultCommentDtos;
	}
	
	
	BooleanExpression getNotSnapshot(){
		QComment comment = QComment.comment;
		return comment.snapshot.eq(false);
	}
	BooleanExpression getReadAccessUser(String textString){
		QComment comment = QComment.comment;
		return comment.bodytext.contains(textString);
	}
	BooleanExpression getReadAccessUGroup(List<String> ugroups){
		
		QComment comment= QComment.comment;
		
		//array를 만들어서 담아 둔다 그리고 나중에 OR로 연결.....
		
		for(String ug : ugroups) {
			comment.raugroups.contains(ug);
		}
		
		return null; //resultBE;
	}
	
	BooleanBuilder getBuilder(Boolean truefalse, String useridstr, List<String> ugroups) {
		QComment comment= QComment.comment;
		BooleanBuilder booleanBuilder1 = new BooleanBuilder();	
		BooleanBuilder booleanBuilder2 = new BooleanBuilder();	
		//ugroups 추가
		for(String ug: ugroups) {
		booleanBuilder1.or(comment.raugroups.contains(ug));
		}
		//user추가
		booleanBuilder1.or(comment.rausers.contains(useridstr));
		//notSnapshot 추가
		booleanBuilder2.and(comment.snapshot.eq(truefalse));
		return booleanBuilder1.and(booleanBuilder2);
	}
	
	
	
	
}
