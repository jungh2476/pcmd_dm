package com.dmalt.pcms.core.secondary.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dmalt.pcms.core.secondary.models.LogRecord;

@Repository
public interface LogRecordRepository extends JpaRepository<LogRecord, Long>  {
	
	Optional<LogRecord> findById(Long id);

}
