package com.dmalt.pcms.core.secondary.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "logrecord")
public class LogRecord {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
	
	@Column
	public Long timestamp; 
	
	@Column
	public String tagsJson;
	
	@Column
	public String level;   //info, warn, debug, 
	
	@Column
	public String description;
	
	@Column
	public Integer value;
	
	public LogRecord(Long time, String tag, String level, String description, Integer value) {
		this.timestamp=time;
		this.tagsJson=tag;
		this.level=level;
		this.description=description;
		this.value=value;
	}
	
	public LogRecord() {}

}
