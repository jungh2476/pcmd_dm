package com.dmalt.pcms.core.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dmalt.pcms.core.models.UserGroup;

public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

}
