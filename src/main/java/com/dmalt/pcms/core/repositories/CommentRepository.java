package com.dmalt.pcms.core.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dmalt.pcms.core.models.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
	
	//Optional<Comment> findById(Long id);
	
	//List<Comment> findByAll();
	
	@Query(value = "SELECT c from Comment c WHERE c.tags LIKE '%:tag%'")   //c.snapshot = 0 AND 
	List<Comment> findByTag(@Param("tag") String tag);
	
	@Query(value = "SELECT c from Comment c WHERE c.snapshot = 0")   //c.snapshot = 0 AND 
	List<Comment> findNotSnapshot();
	
	@Query(value = "SELECT * from comments c WHERE c.snapshot = 0 AND c.tags LIKE %:tag%", nativeQuery = true)  //c.tags LIKE '%:tag%'
	List<Comment> findNativeByTag(@Param("tag") String tag);  //Object[]> @Param("tag") String tag
	//LIKE CONCAT('%',:username,'%')
	
	@Query(value = "SELECT * from comments c WHERE c.snapshot = 0 AND c.tags LIKE CONCAT('%','\"',:tag,'\"','%')", nativeQuery = true)
	List<Comment> findNative2ByTag(@Param("tag") String tag);

}
