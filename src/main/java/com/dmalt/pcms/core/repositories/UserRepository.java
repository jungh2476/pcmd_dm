package com.dmalt.pcms.core.repositories;

import org.springframework.stereotype.Repository;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dmalt.pcms.core.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    // by id (snapshot 여부 상관없이)
	Optional<User> findById(Long id);
    
}
