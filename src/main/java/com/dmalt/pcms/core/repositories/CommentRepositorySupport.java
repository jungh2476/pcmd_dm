package com.dmalt.pcms.core.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.dmalt.pcms.core.models.Comment;
import com.dmalt.pcms.core.models.QComment;
import com.querydsl.jpa.impl.JPAQuery;



@Repository
public class CommentRepositorySupport { //extends QuerydslRepositorySupport {
	
	//public CommentRepositorySupport() {  //JPAQueryFactory queryFactory
	//	super(Comment.class);
		//this.query=queryFactory;
	//}
	
	Logger logger = LoggerFactory.getLogger(CommentRepositorySupport.class);
	
	@PersistenceContext
	public EntityManager em;
	
	//모든 method 마다 반복해야 하는가?
	//JPAQuery<Comment> query = new JPAQuery<>(em);
	//QComment comment = QComment.comment;
	
	public List<Comment> findByTagQueryDSL(String tag){
		JPAQuery<Comment> query = new JPAQuery<>(em);
		QComment comment = QComment.comment;
		return query.from(comment).where(comment.tags.contains(tag)).fetch();
	}
	
	public List<Comment> findByNoteQueryDSL(String note){
		JPAQuery<Comment> query = new JPAQuery<>(em);
		QComment comment = QComment.comment;
		return query.from(comment).where(comment.bodytext.contains(note)).fetch();
	}
	
	//private QComment comment = QComment.comment;
	//private JPAQueryFactory query;
	
	/*
	public List<Comment> findByTag(String tag){
		
		return queryFactory.selectFrom(comment).where(comment.tags.eq(tag)).fetch();
	}
	*/
	
	/*
	public List<Comment> findBy2Tag(String tag){
		QComment comment = QComment.comment;
		
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		
		return getQuerydsl().where(booleanBuilder).fetch();
		
		@SuppressWarnings("unchecked")
		List<Comment> comments = (List<Comment>) query.from(comment)
				.where(comment.tags.contains(tag)).fetch(); //list(comment)				
		return comments;						
		
	}
	*/
	
}
