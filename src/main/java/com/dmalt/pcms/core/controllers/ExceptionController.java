package com.dmalt.pcms.core.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value="/error")
@Controller
public class ExceptionController {
	
	Logger logger = LoggerFactory.getLogger(ExceptionController.class);
	
	@GetMapping(value={"","**","/","/**"})
	public String pringError() {
		logger.info("Iam, Exception controller called!!!");
		return "error/error";
	}

}
