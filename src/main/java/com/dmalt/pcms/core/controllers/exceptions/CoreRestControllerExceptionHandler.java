package com.dmalt.pcms.core.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class CoreRestControllerExceptionHandler {
	
	
	@ExceptionHandler(value = {ResourceNotFoundException.class, RuntimeException.class})  //
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ErrorMessage resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
	   // ErrorMessage message = new ErrorMessage(status,date,ex.getMessage(),description);
	    return null;
	   // return message;
	}

}
