package com.dmalt.pcms.core.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CoreRestControllerAdvice {
	
	//Controller Advice에서 Exception.class를 먼져 잡아 갈수도 있다
	@ExceptionHandler(Exception.class)
	ResponseEntity<String> handle_all_exceptions(Exception exception){
		//return null;
		return new ResponseEntity<>("I am sorry internal error", HttpStatus.BAD_REQUEST);
	}
}
