package com.dmalt.pcms.core.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


//@RequestMapping(value="/sample")   //method = RequestMethod.GET
@RestController
public class SampleController1 {
	
	Logger logger = LoggerFactory.getLogger(SampleController1.class);
	
	@GetMapping("/sample3")
	public String greeting() {
		logger.info("hi");
		return "sample3";
	}
	
}
