package com.dmalt.pcms.core.controllers.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {
	
	 // 400
	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<ErrorResponse> handleConflict(RuntimeException ex, WebRequest request) {
		String bodyOfResponse = "This should be application specific";
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}
	
	public ResponseEntity<ErrorResponse> handleExceptionInternal(RuntimeException ex, String bodyResponse, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
		return null;
	}
	
	 // 401
	//@ExceptionHandler({ AccessDeniedException.class })
	
	// 500
    //@ExceptionHandler({ Exception.class })

}
