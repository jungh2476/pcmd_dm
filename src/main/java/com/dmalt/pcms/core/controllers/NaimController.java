package com.dmalt.pcms.core.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/naim")  //ad.naim.ai
public class NaimController {
	
	Logger logger = LoggerFactory.getLogger(NaimController.class);
	
	//@RequestMapping(value={"","/","*"}, method = RequestMethod.GET)  //  "/*"
	//return "redirect:today"
	
	@ModelAttribute("ga_code")  //Model addAttribute(String name, Object value)
	public String ga_code() {
		return "ta-123456"; // must import from application.properties
	}
	
	
	@RequestMapping(value="/general/{path}")
	public String getnaimPages(@PathVariable("path") String path, Model model, HttpServletRequest request) {
		
		String whatYouCallValue = request.getServletPath();
		logger.info("the ural coming thru is:"+whatYouCallValue);
		
		model.addAttribute("bgUrl","/img/uj/uj.jpg");
		String result="marketplace/index";
		switch(path) {
		case "index":
			result="marketplace/index";
			break;
		
		case "about":
			logger.info("aboutPgae called");
			result="marketplace/about";
			break;
			
		case "catujtest":
			logger.info("testPgae:temp/adnaim.uj called");
			result="temp/adnaim-uj";
			break;
			
		case "product1test":
			logger.info("testPgae:temp/adnaim-product-1 called");
			result="temp/adnaim-product-1";
			break;
			
		case "naim":
			logger.info("testPgae:temp/naim called");
			result="temp/naim";
			break;
		
		default:
			result="marketplace/index";
			break;
		
		
		}
		
		return result;
	}
	
	
	@RequestMapping(value="/ad")
	public String adnaimai() {
		return "marketplace/adnaimai";
	}
	
	@RequestMapping(value="/category/{category_uname}", method = RequestMethod.GET)
	public String getCategoryPage(@PathVariable("category_uname") String category_uname, Model model) throws Exception {
		logger.info("ad category called");
		model.addAttribute("name","john smith");
		return "marketplace/adcategory";
	}
	
	@RequestMapping(value="/product/{product_id}", method = RequestMethod.GET)
	public String getProductPage(@PathVariable("product_id") String product_id, Model model) throws Exception {  //@PathVariable String path, 
		
		model.addAttribute("key","value");  //${key} <span th:utext="${user.name}"></span>
		logger.info("response as webpage from naimeAdController"+product_id);
		//throw new Exception();
		return "marketplace/helloworld";
		//return "marketplace/adproduct";
	}
	
	@RequestMapping(value="/hello")
	public String HelloWorld() {
		return "marketplace/hello";
	}
	
	
	@RequestMapping(value="/content")
	public String getContentDivsDraft() {
		return "marketplace/content_divs_to_apply";
	}
	
	@RequestMapping(value={"/product**","/product/**","/product*","/product/"})
	public String show_all_products_redirect(){
		logger.info("redirecting to product default page");
		return "redirect:/naim/product/1";
	}
	
	@RequestMapping(value = {"","*","**","/**","/*"})
	public String handle_no_matching_urls(Exception exception) {
		
		logger.info("naim_ad_controller found no matching urls!!!");
		return "error/error";
	}
	
	@ExceptionHandler(Exception.class)
	public String handle_naimad_only_exception(Exception exception) {
		
		logger.info("naim_ad_controller exceptionss!!!");
		return "error/error";
	}
	
}
