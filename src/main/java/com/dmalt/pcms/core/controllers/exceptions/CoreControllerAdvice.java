package com.dmalt.pcms.core.controllers.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/*
	@ControllerAdvice(basePackages="com.java,some", basePackageClasses={ Some1.class, Some2.class }, assignableTypes=SomeController.class annotations=SomeAnnotation.class)
	
*/
@ControllerAdvice
public class CoreControllerAdvice {
	
	Logger logger = LoggerFactory.getLogger(CoreControllerAdvice.class);
	
	/*
	 @ExceptionHandler({SomeException.class})
  public ResponseEntity<String> handleSomeException(SomeException pe, HandlerMethod handlerMethod) {
    Class controllerClass = handlerMethod.getMethod().getDeclaringClass();
    //controllerClass.toString will give you fully qualified name
    return new ResponseEntity<>("SomeString", HttpStatus.BAD_REQUEST);
  }
	 */
	
	//@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleInternalError(Exception e) {
		logger.info("CoreController Advice called!!");
		//return "I am sorry internal error";
		return new ResponseEntity<>("I am sorry internal error", HttpStatus.BAD_REQUEST);
	}
	
	//@Order(Ordered.LOWEST_PRECEDENCE)
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(Exception exception) {
        String eMsgString="error happended!!";
        // exception 결과 표시용 객체 처리
        System.err.println(exception.getClass());
        logger.error(eMsgString);
        return new ModelAndView("redirect:/error");
    }
}
