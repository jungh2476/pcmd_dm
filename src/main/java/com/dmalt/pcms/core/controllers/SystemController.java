package com.dmalt.pcms.core.controllers;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dmalt.pcms.core.controllers.exceptions.CoreException1;

@RestController
public class SystemController {
	
	@GetMapping("Sys1/")
	public String show() throws CoreException1 {  // or try_catch(Runtime exception){다른 response...}
		String result="Hello system";
		return result;  //null 결과일때 header에 값을 넣어 주거나, HATEOS로 답변만든다
	}
	
	@ExceptionHandler(CoreException1.class)//{,} 여러개일 경우  //runtime exception or ioexception
	public void exception_handler() {
		//do something
		//return "error_page_content";
		
	}

}
