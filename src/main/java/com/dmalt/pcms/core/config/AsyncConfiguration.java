package com.dmalt.pcms.core.config;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

//@Configuration
//@EnableAsync
public class AsyncConfiguration extends AsyncConfigurerSupport {
	
	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(50);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("spring-boot-mysql-");
        executor.initialize();
        return executor;
	}
	
	@Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new CustomAsyncExceptionHandler();
		//return new SimpleAsyncUncaughtExceptionHandler();
        //A default AsyncUncaughtExceptionHandler that simply logs the exception...다른일 하고 싶으면 custom class bean  만들어야..
        //Interface AsyncUncaughtExceptionHandler -handleUncaughtException(java.lang.Throwable ex, java.lang.reflect.Method method, java.lang.Object... params)
	}
	
	public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
		 
		@Override
		public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
			System.out.println("Exception message - " + throwable.getMessage());
	        System.out.println("Method name - " + method.getName());
	        for (Object param : obj) {
	            System.out.println("Parameter value - " + param);
	        }
			
		}

	

	     
	}
	
}
