package com.dmalt.pcms.core.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "hashtags")
public class Hashtag {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	
	@Column(unique=true)
    public String hashtag;
	
	@Column()
    public String description;
	
	
	
	//optional 
	//description by creator
	//locale
	//scope
	//inheritance from content?
	//createdWhen Who, type = public, private, onlyVisible to scope: some userGroup or org or business

}
