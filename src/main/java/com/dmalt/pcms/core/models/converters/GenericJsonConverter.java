package com.dmalt.pcms.core.models.converters;

import java.io.IOException;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;




public class GenericJsonConverter<T> implements AttributeConverter<T, String> {
	
	Logger logger = LoggerFactory.getLogger(GenericJsonConverter.class);
	private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	//objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	
	@Override
	public String convertToDatabaseColumn(T attribute) {
		try {
			logger.info("generic converter seriazlier is called::");
			return objectMapper.writeValueAsString(attribute);			
		} catch (JsonProcessingException e) {
			logger.error("failed to serialize",attribute,e);
			throw new RuntimeException(e);
		}

	}

	@Override
	public T convertToEntityAttribute(String dbData) {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		logger.info("generic converter deseriazlier is called::");
		try {
			return objectMapper.readValue(dbData, new TypeReference<T>(){});
		} catch (IOException e) {
			logger.error("failed to deserialize into object:{}",dbData,e);
			throw new RuntimeException(e);
		}
		
	}

}
