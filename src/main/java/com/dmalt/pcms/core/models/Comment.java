package com.dmalt.pcms.core.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Data
@Table(name = "comments")
public class Comment extends Content {
	
	@Column(nullable=true)
	String bodytext;
	
	
	public Comment(long creator_id, long updator_id, boolean snapshot, boolean draft, String description) {
		//from content mapped class
		this.authorid = creator_id;
		this.updatedby = updator_id;
		this.snapshot = snapshot;
		this.draft = draft;
		this.bodytext = description;
		
	}
	
	public Comment(){}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
	
	
	

}
