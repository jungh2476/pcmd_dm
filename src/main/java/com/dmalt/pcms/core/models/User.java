package com.dmalt.pcms.core.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Data
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    
    @Email
	@Column(unique=true)
    public String email;
    
    @JsonIgnore  // Rest에서 패스워드 노출되는 것을 막을려면, 이렇게 한다 .., EncodedPassword면 60char이상 값이 저장될것임
	@Size(min = 4)
	@Column(nullable = true)
	public String password;

	@Column
	public String name;  // John Smith
	
	// private String scopeJSON;
	// @Convert(converter = GroupMembersConverter.class)
	@ElementCollection
	public Set<String> scopes = new HashSet<>();
	
	public String temp;

	@Column(unique=true, nullable=true)
	public String uniquename;   // url-mapped
	
	public User(String email, String password, String name, String uname) {
		this.email = email; 
		this.password = password;
		this.name = name;
		this.uniquename = uname;
		//
		this.scopes.add(name);
		this.scopes.add(uname);
	}

	public User() {}
    
}
