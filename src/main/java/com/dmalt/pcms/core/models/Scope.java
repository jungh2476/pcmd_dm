package com.dmalt.pcms.core.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "scopes")  //scope = roles
public class Scope {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public long id;
    
	@Column(unique=true)
    public String scope;
	
	
    
    

}
