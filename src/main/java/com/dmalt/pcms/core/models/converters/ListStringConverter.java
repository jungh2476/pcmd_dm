package com.dmalt.pcms.core.models.converters;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.SegmentedStringWriter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ListStringConverter {
	
	private final static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	
	public static List<String> toListString(String listString){
		try {
			return objectMapper.readValue(listString, new TypeReference<List<String>>(){});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		//return null;
	}
	
	public static List<Long> toListLong(String listLong){
		try {
			return objectMapper.readValue(listLong, new TypeReference<List<Long>>(){});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		//return null;
	}
	
	public static String toStringStringList(List<String> list) {
		try {
			return objectMapper.writeValueAsString(list);			
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		//return null;
	}
	
	public static String toStringLondList(List<Long> list) {
		try {
			return objectMapper.writeValueAsString(list);			
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		//return null;
	}

}
