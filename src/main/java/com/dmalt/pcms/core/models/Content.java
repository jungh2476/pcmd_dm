package com.dmalt.pcms.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


import com.dmalt.pcms.core.models.converters.LongConverter;
import com.dmalt.pcms.core.models.converters.StringConverter;


import lombok.Data;


@MappedSuperclass
@Data
public abstract class Content {
	//e.g. NewClass extends Content {}
	//draft,snaphot, del,(active), editor, createdWhen, lastUpdated, parent_id, set<permission>
	//set<Hashtag>, set<Category>, org_id
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	
	@Column(nullable = true)
	public String modelver;
	
	// deprecated ?
	@Column(nullable = true)
	public long parentid;
	
	
	@Column(nullable = true)
	public long createdtzone;
	@Column(nullable = true)
	public long createdtime;
	@Column(nullable = true)
	public long authorid;
	
	@Column(nullable = true)
	public long updatedzone;
	@Column(nullable = true)
	public long lastmodifiedtime;
	@Column(nullable = true)
	public long updatedby;
	
	@Column(columnDefinition="tinyint(1) default 1", nullable = true) // 0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean pubread;
	@Column(nullable = true)
	public int version;  //snapshot version
	
	@Column(nullable = true)
	public String versionMessage;
	
	
	//@Column(nullable = true)
	//@Convert(converter = AccessControlConverter.class )
	//public AccessControl accControl;
	
	//public String accControlJson;
	
	//@Convert(converter = StringConverter.class)
	@Column(name="rausers", nullable = true)
	public String rausers;
	
	@Column(name="raugroups", nullable = true)
	public String raugroups;
	
	@Column(name="tags", nullable = true)
	public String tags;
	
	//category :  topic : category
	

	
}
