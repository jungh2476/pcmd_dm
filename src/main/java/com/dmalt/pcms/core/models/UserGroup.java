package com.dmalt.pcms.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;

@Entity
@Data
@Table(name = "ugroups")
public class UserGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public long id;
    
	
	@Column(unique=true)
    public String uname;
	
	@Column(nullable=true)
    public String description;
	
	//One2Many
	//List<User> users = new ArrayList<>();
	

}
