package com.dmalt.pcms.core.models.converters;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.dmalt.pcms.core.models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class UserAccessListConverter implements AttributeConverter<List<User>, String>  {
	public ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public String convertToDatabaseColumn(List<User> users) {
		if (users == null) {
            return null;
        }
		String usersString="";
		try {
			usersString = objectMapper.writeValueAsString(users);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return usersString;
	}

	@Override
	public List<User> convertToEntityAttribute(String usersjson) {
		if (usersjson == null || usersjson.isEmpty()) {
            return null;
        }
		
		ObjectMapper objMapper = new ObjectMapper();
		//List<User> users = new ArrayList<>(); //User() {};
		List<User> users = null;
		try {
			users = objMapper.readValue(usersjson, new TypeReference<List<User>>(){});
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//User user = new User() {};
		//users.add(user);
		
		
		return users;
	}

}
