package com.dmalt.pcms.core.models.dtos;


import com.querydsl.core.annotations.QueryProjection;

import lombok.Data;

@Data
public class CommentDto {

	public String name;
	public String tags;
	public String bodytext;
	
	@QueryProjection
	public CommentDto(String tags,String bodytext,String name){
		this.name=name;
		this.tags=tags;
		this.bodytext=bodytext;
	}
	
	@Override
	public String toString(){
		String resultString="name:"+this.name+",tags:"+this.tags.toString()+",bodytext:"+this.bodytext;
		return resultString;
	}
	
}
