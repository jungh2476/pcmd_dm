package com.dmalt.pcms.core.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
public class AccessControl {

	public List<Long> readAccessUsers = new ArrayList<>();
	public List<Long> readAccessUserGroups = new ArrayList<>();
	public List<Long> writeAccessUsers = new ArrayList<>();
	public List<Long> writeAccessUserGroups  = new ArrayList<>();
	
	public AccessControl() {}
	
	public AccessControl(List<Long> rUsers, List<Long> wUsers) {
		this.readAccessUsers=rUsers;
		this.writeAccessUsers=wUsers;
	}
	
	
}
