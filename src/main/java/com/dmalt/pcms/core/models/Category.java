package com.dmalt.pcms.core.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "categories")
public class Category {
	
	public static final int JOURNAL=1;
	public static final int NAME=2;
	public static final int ORG_1=31;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    
	@Column()
    public int super_category;  //select one from the static variables above
	
	@Column(nullable=true)
    public String category;

}
