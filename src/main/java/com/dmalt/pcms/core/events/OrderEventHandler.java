package com.dmalt.pcms.core.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class OrderEventHandler {
	
	Logger logger = LoggerFactory.getLogger(OrderEventHandler.class);
	
	//@SneakyThrows
	@Async
	@EventListener
	public void handleEvent(OrderEvent orderEvent) {
		//
		logger.info("orderEvent received1:");
		logger.info("orderEvent received2:"+orderEvent.data);
	}

}
