package com.dmalt.pcms.task.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/sample")
public class SampleController {
	
	//curl -i http://localhost:8080/spring-rest/ex/foos
	@GetMapping
	public String getIndexSample(Model model) {
		String page="sample";
		List<String> obj = new ArrayList<>();
		model.addAttribute("keyName",obj);
		return page;
	}
}

/*
 현재 경로에서 위치 참조 : /test
 1. @{/sample1} => /test/sample1
 2. @{~/sample1} => /sample1 
 3. @{/sample1(p1='aaa')} => /sample1?p1='aaa'
 */
