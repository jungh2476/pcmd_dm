package com.dmalt.pcms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.dmalt.pcms.core.repositories.UserRepository;


//@ExtendWith(MockitoExtension.class) 
@ExtendWith(SpringExtension.class)
public class SampleTest {
	
	@MockBean
	UserRepository uRepo;
	
	//@InjectMocks
	//Mockito.when().ThenReturn()....
	//@Before
	//public void setUp() {
	
	Logger logger = LoggerFactory.getLogger(SampleTest.class);
	
	//@RunWith no longer exists, superseded by the extension model using @ExtendWith
	//use @BeforeEach and @AfterEach instead,  use @BeforeAll and @AfterAll
	//@Ignore no longer exists, use @Disabled
	//assertNotNull(result);
	//assertThrows(NumberFormatException.class, () -> Integer.valueOf("duke"));
	
	//"true", 이것이 있어도 그냥 실행되었음 expression = "${tests.enabled}", loadContext = true
	@EnabledIf(value = "false")  
	@Test
	public void junit5_simple_test() {
		
		logger.info("junit test sampled is executed!!!!");
		
		int result = 200;
		Assertions.assertEquals(200, result);
	}
	
	@Disabled("Disabled until CustomerService is up!")
	@Tag("integration")    //mvn -DexcludeTags={tag names}, https://keyholesoftware.com/2018/02/12/disabling-filtering-tests-junit-5/
	@Test
	public void unitTest2() {
		
		logger.info("2nd junit test sampled is executed");
		
		int result = 200;
		Assertions.assertEquals(200, result);
		
	}
}
