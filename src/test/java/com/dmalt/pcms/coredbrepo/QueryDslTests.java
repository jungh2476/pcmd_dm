package com.dmalt.pcms.coredbrepo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.dmalt.pcms.core.models.Comment;
import com.dmalt.pcms.core.models.QComment;
import com.dmalt.pcms.core.models.User;
import com.dmalt.pcms.core.models.converters.ListStringConverter;
import com.dmalt.pcms.core.models.dtos.CommentDto;
import com.dmalt.pcms.core.repositories.CommentRepository;
import com.dmalt.pcms.core.repositories.CommentRepositorySupport;
import com.dmalt.pcms.core.repositories.UserGroupRepository;
import com.dmalt.pcms.core.repositories.UserRepository;
import com.dmalt.pcms.core.services.CommentService;
import com.querydsl.jpa.impl.JPAQuery;



//@Transactional
@SpringBootTest
public class QueryDslTests {
	
	Logger logger = LoggerFactory.getLogger(QueryDslTests.class);
	
	@Autowired
	CommentRepository commentRepo;
	@Autowired
	CommentRepositorySupport commentRepoSupport;
	@Autowired
	CommentService commentServ;
	@Autowired
	UserRepository uRepo;
	@Autowired
	UserGroupRepository ugRepo;
	
	@Test
	public void initialQdslRepotest() {
		Comment comment1= new Comment(3, 45, false, false, "hello description1");
		List<String> rausers=  new ArrayList<>();
		rausers.add("1");
		rausers.add("3");
		String rausersString=ListStringConverter.toStringStringList(rausers);
		comment1.rausers=rausersString;
		List<String> raugroups=  new ArrayList<>();
		raugroups.add("1");
		raugroups.add("3");
		String raugroupsString=ListStringConverter.toStringStringList(raugroups);
		comment1.raugroups=raugroupsString;
		List<String> tags2 = new ArrayList<>();
		tags2.add("tag1tag2");
		tags2.add("tag2tag3");
		tags2.add("tag3");
		String tag2String=ListStringConverter.toStringStringList(tags2);
		comment1.tags=tag2String;
		commentRepo.save(comment1);
		Comment comment2= new Comment(5, 35, true, false, "hello description2");
		rausers.add("5");
		rausers.add("7");
		String rausers2String=ListStringConverter.toStringStringList(rausers);
		comment2.rausers=rausers2String;
		raugroups.add("5");
		raugroups.add("9");
		String ragroups2String=ListStringConverter.toStringStringList(raugroups);
		comment2.raugroups=ragroups2String;
		comment2.tags=tag2String;
		commentRepo.save(comment2);
		
		logger.info("initial queryDSL comment Repo support test starting....");
		List<Comment> comments=commentRepoSupport.findByNoteQueryDSL("description");
		logger.info("matched result size is:"+comments.size());
		assertThat(comments.size()).isEqualTo(2);
		
	}
	
	//@Transactional
	@Test
	public void plainemusetest() {
		
		logger.info("2nd queryDSL test starting.....");
		//테스트에서는 entitymanager 불러서 코딩하는 것이 안되어서 service나 repoSupport로 옮겨서 method로 실행하여 동작시켯음
		List<Comment> comments = commentServ.findByBodytextDescriptionQueryDSL();
		logger.info("the size matched 'description' is : "+comments.size());
		//AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PrimaryDataSourceConfiguration.class);
		//EntityManagerFactory emf= context.getBean(EntityManagerFactory.class); //= Persistence.createEntityManagerFactory("com.dmalt.pcms.core.models");
		//EntityManager em = emf.createEntityManager();
		
		
		//comment_em.getTransaction().commit();
		//emf.close();
		
		
	}
	
	@Test
	void getContentBySnaphotAndReadAccessWithDTO(){
		
		//user_id로 읽기 권한인 ㅣㅇㅆ는 경우, ug에 3군데 속해있어서 모든 그룹에 대한 접근 권한이 있는지 체크하는 루틴도 시도
		
		//save
		//users : u1, u2, u3
		//ugroups: ug1
		//comments(snapshot,pubread,ua,uag,tags) : comment1(0-false,0-false,u1,ug1, tag1), comment2(1-true,, comment3, 
		logger.info("starting savibg");
		User u1 = new User("jane11@g.com","1234sd","jane3","jane11" ); //email, password(4), name, uniquename
		uRepo.save(u1);
		User u2 = new User("jane12@g.com","1234sd","jane4","jane12" );
		uRepo.save(u2);
		User u3 = new User("jane13@g.com","1234sd","jane5","jane13" ); //email, password(4), name, uniquename
		uRepo.save(u3);
		User u4 = new User("jane14@g.com","1234sd","jane6","jane14" );
		uRepo.save(u4);
		//find & map-to DTO
		
		List<CommentDto> commentDtos = commentServ.findAllByTagDTO("tag1", "23", "ug1"); //Long.getLong("23")
		logger.info("result commentDto macthed size : "+commentDtos.size());
		if(commentDtos.size()>0) {logger.info("1st resuult:"+commentDtos.get(0).toString());}
		logger.info("bye bye");
		
	}
	
	//@AfterTestClass
	//public static void afterClass() {


	
}
