package com.dmalt.pcms.coredbrepo;


import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.dmalt.pcms.core.models.converters.ListStringConverter;

@ExtendWith(SpringExtension.class)
public class ListStringTests {
	
	Logger logger = LoggerFactory.getLogger("ListStringTests.class");
	
	@Test
	public void conversion_simpleTest(){
		
		List<String> listStr=new ArrayList<>();
		listStr.add("A");
		listStr.add("B");
		listStr.add("C");
		listStr.add("D");
		
		String resultString="";
		resultString=ListStringConverter.toStringStringList(listStr);
		logger.info("resultString is :"+resultString);
		
		List<String> listResult=ListStringConverter.toListString(resultString);
		logger.info("4:"+listResult.size());
		if(listResult.size()>0) {logger.info("3rd item:"+listResult.get(2));}
	}
}
