package com.dmalt.pcms.coredbrepo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.dmalt.pcms.core.models.Comment;
import com.dmalt.pcms.core.models.converters.ListStringConverter;
import com.dmalt.pcms.core.models.converters.LongConverter;
import com.dmalt.pcms.core.models.dtos.CommentDto;
import com.dmalt.pcms.core.repositories.CommentRepository;
import com.dmalt.pcms.core.services.CommentService;

@Disabled
@SpringBootTest
public class AccessControlTests {
	
	Logger logger = LoggerFactory.getLogger(AccessControlTests.class);
	
	
	@Autowired
	CommentRepository commentRepo;
	
	@Autowired
	CommentService commentServ;
	
	
	@Test
	void accControlCommentsave() {
		
		logger.info("comment accControl test started");
		/*
		AccessControl accControl1 = new AccessControl() {};
		accControl1.readAccessUsers.add(Long.valueOf(1L));
		accControl1.readAccessUsers.add(Long.valueOf(12354L));
		accControl1.readAccessUsers.add(Long.valueOf(312321L));
		accControl1.writeAccessUsers.add(Long.valueOf(1L));
		accControl1.writeAccessUserGroups.add(Long.valueOf(12354L));
		accControl1.readAccessUserGroups.add(Long.valueOf(312321L));
		*/
		List<String> tags = new ArrayList<>();
		tags.add("tag1");
		tags.add("tag2");
		tags.add("tag3");
		String tagsString = ListStringConverter.toStringStringList(tags);
		List<String> tags2 = new ArrayList<>();
		tags2.add("tag1tag2");
		tags2.add("tag2tag3");
		tags2.add("tag3");
		String tags2String = ListStringConverter.toStringStringList(tags2);
		Comment comment1= new Comment(12, 45, false, false, "hello description");
		//comment1.accControl=accControl1;
		
		comment1.tags=tagsString;
		
		Comment commentResultComment = commentRepo.save(comment1);
		logger.info("comment id saved : "+commentResultComment.id);
		Comment comment2= new Comment(66, 454, false, false, "hello2 description");
		//comment2.accControl=accControl1;
		comment2.tags=tags2String;
		commentRepo.save(comment2);
		logger.info("accControl saving complated");
		
		
		
		/*
		 Optional<Comment> ocomment= commentRepo.findById(commentResultComment.id); //Long.valueOf(1L)
		 
		Comment comment2 = ocomment.get();
		AccessControl accessControl2=comment2.accControl;
		
		logger.info("accessControl of 1L is : "+accessControl2.readAccessUsers.toString());
		*/
	}
	
	@Test
	void getContentFromRepo(){
		
		logger.info("accControl get findby test started");
		List<Comment> comments = commentRepo.findAll();
		logger.info("1st result is "+comments.get(0));
		
		List<Comment> comments2=commentRepo.findNotSnapshot();
		logger.info("2nd result is size: "+comments2.size());
		String tagString1="tag1";  //"\"tag1\"";
		String tagString2="\""+tagString1+"\"";  //정확히 ""에 쌓여있는 그 단어만 들어있는 내용 찾아 내기
		List<Comment> resultList=commentRepo.findNativeByTag(tagString2);  //Object[] tagString
		//List<Comment> comments3=resultList.stream().map()
		//List<Comment> comments3=commentRepo.findNativeByTag(tagString);
		logger.info("3rd result is size: "+resultList.size());
		//logger.info("3rd result is size: "+comments3.size());
		logger.info("3rd result is result: "+resultList.get(0));
		
		List<Comment> comments4=commentRepo.findNative2ByTag(tagString1);
		logger.info("4th result is size: "+comments4.size());
		
	}
	
	
	
	
}
