package com.dmalt.pcms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.dmalt.pcms.core.models.User;
import com.dmalt.pcms.core.repositories.UserRepository;
import com.dmalt.pcms.core.secondary.models.LogRecord;
import com.dmalt.pcms.core.secondary.repositories.LogRecordRepository;
import com.dmalt.pcms.core.services.UserService;

@Disabled
@SpringBootTest
class PcmsApplicationTests {
	
	Logger logger = LoggerFactory.getLogger(PcmsApplicationTests.class);

	@Autowired
	UserRepository uRepo;
	@Autowired
	UserService uServ;
	@Autowired
	LogRecordRepository lRepo;
	
	@Value("${spring.version}")
	private String swVersion;
	
	@Test
	void contextLoads() {
		logger.info("swversion import from application.properties is: "+ swVersion);
		String name = "alex";
		assertThat(name).isEqualTo("alex");
	}
	
	
	@Transactional  //테스트에서 같이 쓰면 종료하면서 rollback 한다
	@Test
	void dbload() {
		logger.info("starting savibg");
		User u1 = new User("jane2@g.com","1234sd","jane","jane1" ); //email, password(4), name, uniquename
		uRepo.save(u1);
		logger.info("jane saving completed");
		
		logger.info("u1 id is : "+u1.id);
		
		Optional<User> u2o = uRepo.findById(u1.id); //Long.valueOf(0L)
		User u2=u2o.get();
		logger.info("now u2 is obtained:");
		logger.info("u2 uname is: "+ u2.uniquename);
		logger.info("u1 repor of scopes set is : "+u2.scopes.contains("jane"));
		
	}
	
	@Test
	void dbLoad2() {
		logger.info("stating dbLoad2 test with smith@g.com");
		User u3 = new User("smith@g11.com","1234sd","sm21ith","smi21th1" ); //email, password(4), name, uniquename
		uRepo.save(u3);
		logger.info("smith saving completed");
		
		User u4 = uServ.findById(u3.id);
		logger.info("now u4 is obtained:");
		logger.info("u4 uname is: "+ u4.uniquename);
		logger.info("u4 scopes size: : "+u4.scopes.size());
		
		
	}
	
	@Test
	void secondary_dbLoad() {
		logger.info("stating secondary db loading testgin");
		LogRecord logRecord = new LogRecord(Long.valueOf(123456L),"tag1","levelinfo","이정환description",Integer.valueOf(3));
		//Long time, String tag, String level, String description, Integer value.... Long l=1L
		lRepo.save(logRecord);
		logger.info("now secondary log db is tested:");
	}

}
