package com.dmalt.pcms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.test.context.junit.jupiter.SpringExtension;

//@SpringBootTest
@Disabled
@ExtendWith(SpringExtension.class)
public class DbTests {
	
	Logger logger = LoggerFactory.getLogger(DbTests.class);
	
	//@Autowired
	//commentRepo
	
	@Test
	void dbCommentsLoads() {
		logger.info("commentRepo loaded");
		logger.info("Dbtesting is executed");
		
		int result = 200;
		Assertions.assertEquals(200, result);
		
	}

}
