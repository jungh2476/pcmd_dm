i18n
resources/ messages_ko_KR.properties
spring.messages.basename=message/messages
spring.messages.encoding=UTF-8

@Autowired
    MessageSource messageSource;  //mS.getMessage("key","input",Locale)


messageSource.getMessage("greeting", new String[]{"Leica"}, Locale.getDefault())
messageSource.getMessage("greeting", new String[]{"Leica"}, Locale.KOREA)
messageSource.getMessage("error.404", null, LocaleContextHolder.getLocale())

"#{key}"


Locale  설정 및 파악 관련
@Configuration
public class LocaleConfiguration implements WebMvcConfigurer {
@Bean
public LocaleResolver localeResolver() {
    CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
    cookieLocaleResolver.setDefaultLocale(Locale.KOREAN);
    cookieLocaleResolver.setCookieName("THYMELEAF_LANG");
    return cookieLocaleResolver;
}

//xxxx.com/xxx?lang=ko를 호출하면 한국어로 변경, xxxx.com/xxx?lang=en 를 호출하면 영어로 변경됨.
@Bean
public LocaleChangeInterceptor localeChangeInterceptor() {
    LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
    interceptor.setParamName("lang");
    return interceptor;
}

-------------------------------------


